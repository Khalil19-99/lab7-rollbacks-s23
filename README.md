# Lab7 - Rollbacks and reliability

## Introduction

Probably you've already worked with reliability at Distributed Systems course. So today we are going to talk about rollback implementation in software.(Though I think you've worked with it as well, but maybe don't know about it) ***Let's roll!***

## Rollbacks

If you have a very complicated or very important function that you need to execute it is a good thing to implement Rollbacks there. You are basically saving the state of the execution, and you are returning to it in case of faliure on the next execution state, to try execute next step once again or return the results of at least previous step. One of the most used areas is databases, when you have a chunk of requests and you want all of them to pass, or none of them, so you are sending them in one commit, and if failure happens, your changes from commit are being removed.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab9 - Rollbacks and reliability
` repo and clone it.
2. We are going to write some code today, our task is to make some backend for in-game shop. Instances that we should manage as follows: players (their names and balance), shop itself (limited only to list of products, their price and quantity in stock) plus for homework your task will be to implement inventory of a player. We will be storing all our data in databse, lets use for example postgresql.
+ Install postgresql(If you have Ubuntu/Mint you should already have it installed)
+ Now, let's open postgresql and create DB like this:
```sql
CREATE DATABASE lab;
\c lab
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
```
+ Pay attention to constraints that we have for created table. Player balance, price and amount of product in stock cannot be negative. 
+ Ofcourse for testing we need some sample data, lets fill in tables with it:
```
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO PLayer (username, balance) VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```
+ Ok, then you have some code in lab, for todays lab we will use python, but you may use any programming language you like. There is the single function `buy_product`, that does all the staff.

How does it works? First of all in the beginning we have import of library that works with postgresql. A bit bellow there is connection code to connect to database and templates of our queries to database. psycopg2 automatically creates, finishes and rollbacks transactions.

There is also `try-except` blocks insyde. Actually there is two ways of checking that there is no such player or balance is to low: you may get this data using multiple select queries (`SELECT balance FROM Player WHERE username = %s` and `SELECT price FROM Shop WHERE product = %s`) and check it in code, or you may rely on checks in database (if constraints that we created for tables failed - psycopg2 raises `CheckViolation` exception and we may handle it) as shown in example below and check count of updated rows. 

## Homework (solutions)

### Part 1 (2/3 points)

After excuting the code, that's the proof of the result of the tables:

![plot](./images/results.png)

considering Bob had 200 balance and there was 10 marhmillo initially.
I modified the `buy_product()` function so it does not lead to data inconsistencies.

### Part 2 (3/3 points)

I created new table for inventory, and added queries to read, update, etc. 
I included inventory checks about the terms (ex: if amount >100 throw exception), and updates in the transaction.

